class NewsletterForm {
  constructor() {
    this.controlVideoPlayback()
  }

  controlVideoPlayback() {
    let modalNewsletter = document.getElementById('modal-newsletter')
    let observer = new MutationObserver(function(event) {
      let video = document.getElementById('video-1')
      let modalClassList = event[0].target.classList

      if (modalClassList.contains('modal--is-showing')) {
        video.classList.remove('video--is-playing')
        video.classList.add('video--is-paused')
        video.playerInstance.pause()
      } else {
        video.classList.remove('video--is-paused')
        video.classList.add('video--is-playing')
        video.playerInstance.play()
      }
    })

    observer.observe(modalNewsletter, {
      attributes: true,
      attributeFilter: ['class'],
      childList: false,
      characterData: false,
    })
  }

  /* Start PseudoCode:
  onFormSubmit(){
    get form field values
    validData = validateFormFieldValues(fields)

    if(validData){
      use api utilities
      try{
        api.post(data)
      } catch (error){
        hide form 
        show error message to the user
      }

      if(api post was successful){
        closeModal
        showSuccessToast
      }
    }
  }

  validateFormFieldValues(field){
    call methods on form-validation utilities to validate and sanitize values
    if(valid){
      return data
    } else (
      return null
    )
  }
  PseudoCode End */
}

export default new NewsletterForm()
