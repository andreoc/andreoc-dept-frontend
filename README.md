# [frontend-assignment](https://bitbucket.org/tamtam-nl/dtnl-dept-frontend-setup-assignment/)

__Notes & Considerations__

- I really liked the framework built for this assignment. It took me a while to get my head around it, but after understanding how it works it was easy to develop – also to organize what I built;

- Easy to find what I’m looking for after understanding folder structure; 

- First time working with Babel and Nunjucks;
    - On that note, it would be helpful to know easily check with what I’m working with, and where to find sources for documentation / help / tips, but I took the opportunity with the mindset that it would be part of the challenge;

- Had some issues when trying to install new libraries, or sometimes, just to run the project. Continuing using node v12 and clean installs helped some of the time, but the best bet was to downgrade to node v8;

- In relation to the Newsletter component, I took the liberty to make the decision that the First Name, Last Name and Email fields are always required for forms. If I had bit more time, I would give the option to choose which fields to show in the form directly in the template, as a macro.


# Install #
Use the setup following these commands.

**When using nvm make sure this is not installed through brew, because this can result in errors when running npm scripts.**

__1. Install all the npm modules__

**Recommendation on Windows:** Make sure to be using Node v8.x, might save some trouble with compatibilitie issues.

`npm install`

__2. Start the project__

`npm run gulp` or  `npm run start`
